#!/bin/sh

# If you are executing this script on Void Linux, these are the prerequisited packages and commands.
# Use this as reference for any non-deb distro. 

sudo mkdir -p /etc/apt/apt.conf.d

sudo xbps-install -Su debootstrap apt fakeroot syslinux qemu 

# Request package this on xbps:
# sudo xbps-install -Su mbr-install


# Add Devuan keyring:
sudo apt-key adv --keyserver keys.gnupg.net --recv BB23C00C61FC752C


